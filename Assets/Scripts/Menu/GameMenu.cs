﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameMenu : MonoBehaviour
{
    public static GameMenu instance;

    public GameObject pauseMenu;
    public GameObject upgradeMenu;
    public GameObject optionsMenu;

    public Text xpText;

    public Text offenseText;
    public Button offenseButton;

    public Text defenseText;
    public Button defenseButton;

    public Text mobilityText;
    public Button mobilityButton;

    public GameObject offenseSkillUI;
    private Animator offenseAnim;
    public GameObject mobilitySkillUI;
    private Animator mobilityAnim;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;

        offenseAnim = offenseSkillUI.GetComponentInChildren<Animator>();
        mobilityAnim = mobilitySkillUI.GetComponentInChildren<Animator>();

        CloseEveryMenu();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            if (!pauseMenu.activeSelf)
                OpenMenu(pauseMenu);
            else CloseEveryMenu();
        }
        if (Input.GetButtonDown("Upgrade"))
        {
            if (!upgradeMenu.activeSelf)
                OpenMenu(upgradeMenu);
            else CloseEveryMenu();
        }
    }

    public void StartOffenseCooldownAnim(Entity entity)
    {
        offenseAnim.gameObject.SetActive(true);
        offenseAnim.speed = 1 / entity.shootCooldown;
        offenseAnim.SetTrigger("StartCooldown");
    }

    public void StartMobilityCooldownAnim(Entity entity)
    {
        mobilityAnim.gameObject.SetActive(true);
        mobilityAnim.speed = 1 / (entity.dashCooldown + entity.dashTime);
        mobilityAnim.SetTrigger("StartCooldown");
    }
    
    public void UpdateSkillsUI(Entity entity)
    {
        //Offense UI
        if (entity.canShoot)
        {
            offenseSkillUI.SetActive(true);
        }
        else
        {
            offenseSkillUI.SetActive(false);
        }

        //Defense UI
        if (entity.canDash)
        {
            mobilitySkillUI.SetActive(true);
        }
        else
        {
            mobilitySkillUI.SetActive(false);
        }
    }

    public void UpdateXPInfo(Entity entity)
    {
        xpText.text = ((int)(entity.currentXP)).ToString();
        UpdateUpgradesInfo(entity.GetComponent<EntityUpgrades>());
    }

    public void UpdateUpgradesInfo(EntityUpgrades upgrades)
    {
        offenseText.text = upgrades.offPrice.ToString();
        if (upgrades.offPrice > upgrades.entity.currentXP || upgrades.offenseLevel >= upgrades.maxOffLevel)
            offenseButton.interactable = false;
        else offenseButton.interactable = true;

        defenseText.text = upgrades.defPrice.ToString();
        if (upgrades.defPrice > upgrades.entity.currentXP || upgrades.defenseLevel >= upgrades.maxDefLevel)
            defenseButton.interactable = false;
        else defenseButton.interactable = true;

        mobilityText.text = upgrades.mobPrice.ToString();
        if (upgrades.mobPrice > upgrades.entity.currentXP || upgrades.mobilityLevel >= upgrades.maxMobLevel)
            mobilityButton.interactable = false;
        else mobilityButton.interactable = true;

        UpdateSkillsUI(upgrades.entity);
    }

    public void QuitGame()
    {
        SceneManager.LoadScene(0);
    }

    public void CloseEveryMenu()
    {
        pauseMenu.SetActive(false);
        upgradeMenu.SetActive(false);
        optionsMenu.SetActive(false);
        Time.timeScale = 1f;
    }

    public void OpenMenu(GameObject menu)
    {
        CloseEveryMenu();
        menu.SetActive(true);
        Time.timeScale = 0f;
    }
}
