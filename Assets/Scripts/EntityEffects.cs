﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityEffects : MonoBehaviour
{
    public bool dashEffectActive;
    public Transform dashEchoPrefab;
    public float dashEchoSpawnDelay = .1f;
    private float dashEchoSpawnTime;
    private float dashEffectStopTime;

    private void Update()
    {
        DashEffect();
    }

    private void DashEffect()
    {
        if (!dashEffectActive)
            return;

        if (dashEffectStopTime <= Time.time) {
            dashEffectActive = false;
            return;
        }

        if (dashEchoSpawnTime <= Time.time)
        {
            Transform newEcho = Instantiate(dashEchoPrefab, transform.position, Quaternion.identity);
            newEcho.localScale = transform.localScale;
            Destroy(newEcho.gameObject, newEcho.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length - .1f);

            dashEchoSpawnTime = Time.time + dashEchoSpawnDelay;
        }
    }

    public void StartDashEffect(float time)
    {
        dashEffectActive = true;
        dashEchoSpawnTime = Time.time + dashEchoSpawnDelay / 2f;
        dashEffectStopTime = Time.time + time;
    }
}
