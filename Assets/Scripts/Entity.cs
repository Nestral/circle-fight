﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]
public class Entity : MonoBehaviour
{
    //Other
    public enum EntityType { Friend, Enemy };
    public EntityType entityType = EntityType.Friend;

    public Vector2 defaultScale = new Vector2(.3f, .3f);
    public Vector2 scaleInc = new Vector2(.1f, .1f);
    
    public float currentXP = 0;
    public float maxHP = 10;
    public float startHP = 10;
    public float currentHP = 10;

    //Movement    
    public float moveSpeed = 5f;
    private float currentSpeed;
    public bool canDash = false;
    public float dashSpeed = 10f;
    public float dashTime = 1f;
    public float dashCooldown = 1f;
    private bool isDashing = false;
    public bool isDashAvailable = true;
    private float dashCooldownTime;

    //Shooting
    public bool canShoot = false;
    public bool isShootAvailable = true;
    public Transform bulletPrefab;
    public float shootDamage = 1f;
    public float shootSpeed = 5f;
    public float shootCooldown = 1f;
    private float shootCooldownTime;

    //Reproducing
    public int reproducePrice = 15;

    //Defense
    public Transform shieldsHolder;
    public float shieldSpeed = 180f;
    public float shieldDistance = 1f;

    private Rigidbody2D rb;
    private PlayerController playerController;
    private EnemyController enemyController;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();

        Controller controller = GetComponent<Controller>();
        if (controller.GetType() == typeof(PlayerController))
            playerController = GetComponent<PlayerController>();
        else if (controller.GetType() == typeof(EnemyController))
            enemyController = GetComponent<EnemyController>();
    }

    private void Start()
    {
        currentSpeed = moveSpeed;
        currentHP = startHP;
        UpdateScale();

        shieldsHolder.GetComponent<ShieldsHolder>().Setup(this, shieldSpeed, shieldDistance);

        if (playerController != null)
        {
            GameMenu.instance.UpdateXPInfo(this);
        }
    }

    public void AddXP(float _xp)
    {
        currentXP += _xp;
        Heal(_xp / 2f);

        if (playerController != null)
            GameMenu.instance.UpdateXPInfo(this);
    }

    public void SpendXP(float _xp)
    {
        if (currentXP < _xp)
            return;

        currentXP -= _xp;
        if (currentXP < 0)
            currentXP = 0;

        if (playerController != null)
            GameMenu.instance.UpdateXPInfo(this);
    }

    public void Heal(float healHP)
    {
        currentHP += healHP;
        if (currentHP > maxHP)
            currentHP = maxHP;
        UpdateScale();
    }

    public void DealDamage(float damage)
    {
        currentHP -= damage;
        if (currentHP <= 0)
        {
            Die();
        }

        UpdateScale();
    }

    private void UpdateScale()
    {
        Vector2 scale = defaultScale + scaleInc * (currentHP - 1);
        transform.localScale = scale;
    }

    private void Die()
    {
        WorldGenerator.instance.SpawnFood(transform.position, currentXP);

        Destroy(gameObject);
    }

    private void Update()
    {
        CheckDash();
        CheckShoot();
    }

    private void CheckDash()
    {
        if (!isDashAvailable)
        {
            if (dashCooldownTime <= Time.time)
            {
                if (isDashing)
                    StopDash();
                else StopDashCooldown();
            }
        }
    }

    private void CheckShoot()
    {
        if (!isShootAvailable)
        {
            if (shootCooldownTime <= Time.time)
            {
                StopShootCooldown();
            }
        }
    }

    public void Move(Vector2 dir)
    {
        dir.Normalize();
        rb.velocity = currentSpeed * dir;
    }

    public void Dash()
    {
        if (!canDash || !isDashAvailable)
            return;

        isDashAvailable = false;
        isDashing = true;

        //Increase speed
        currentSpeed = dashSpeed;

        if (playerController != null)
        {
            //Start cooldownAnim
            GameMenu.instance.StartMobilityCooldownAnim(this);
        }

        //Start dash timer
        dashCooldownTime = Time.time + dashTime;
    }

    private void StopDash()
    {
        currentSpeed = moveSpeed;
        isDashing = false;

        //Start cooldown timer
        dashCooldownTime = Time.time + dashCooldown;
    }

    private void StopDashCooldown()
    {
        isDashAvailable = true;
    }

    public void Shoot(Vector2 dir)
    {
        if (!canShoot || !isShootAvailable)
            return;

        dir.Normalize();

        isShootAvailable = false;

        SpawnBullet(dir);

        if (playerController != null)
        {
            //Start cooldownAnim
            GameMenu.instance.StartOffenseCooldownAnim(this);
        }

        //Start cooldown timer
        shootCooldownTime = Time.time + shootCooldown;
    }

    private void StopShootCooldown()
    {
        isShootAvailable = true;
    }

    private void SpawnBullet(Vector2 shootDir)
    {
        Transform newBullet 
            = Instantiate(bulletPrefab, transform.position, Quaternion.identity, WorldGenerator.instance.spawnsHolder);
        newBullet.GetComponent<Rigidbody2D>().velocity = shootSpeed * shootDir.normalized;

        Bullet bullet = newBullet.GetComponent<Bullet>();
        bullet.type = entityType;
        bullet.damage = shootDamage;
    }

    public void Reproduce()
    {
        if (currentXP < reproducePrice)
            return;

        SpendXP(reproducePrice);
        DealDamage(currentHP / 3f);

        if (enemyController != null)
            WorldGenerator.instance.SpawnEntity(enemyController, entityType, transform.position, currentHP, 0);
        else WorldGenerator.instance.SpawnEntity(null, entityType, transform.position, currentHP, 0);
    }
}
