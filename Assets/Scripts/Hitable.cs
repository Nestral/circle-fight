﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public abstract class Hitable : MonoBehaviour
{
    public Vector2 defaultScale = new Vector2(.3f, .3f);
    public Vector2 scaleInc = new Vector2(.1f, .1f);

    public float sizeDecSpeed = 100f;
    public float minSize = .1f;

    private void Awake()
    {
        SetSize();
    }

    internal abstract void SetSize();

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.isTrigger && !(other.CompareTag("Entity") || other.CompareTag("Shield")))
        {
            Destroy(gameObject);
            return;
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Entity"))
        {
            StayInEntity(other);
            SetSize();
        }
    }

    internal abstract void StayInEntity(Collider2D other);
}
