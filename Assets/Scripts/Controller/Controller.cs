﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Entity), typeof(EntityUpgrades), typeof(EntityEffects))]
public abstract class Controller : MonoBehaviour
{
    internal Entity entity;
    internal EntityEffects effects;

    internal virtual void Awake()
    {
        entity = GetComponent<Entity>();
        effects = GetComponent<EntityEffects>();
    }

    private void Update()
    {
        if (Time.timeScale != 0)
            TakeInput();
    }

    internal abstract void TakeInput();

    internal void Move(Vector2 dir)
    {
        entity.Move(dir);
    }

    internal void Dash()
    {
        if (!entity.canDash || !entity.isDashAvailable)
            return;

        entity.Dash();
        effects.StartDashEffect(entity.dashTime);
    }

    internal void Shoot(Vector2 dir)
    {
        entity.Shoot(dir);
    }

    internal void Reproduce()
    {
        entity.Reproduce();
    }
}
