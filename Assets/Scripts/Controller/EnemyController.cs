﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemyController : Controller
{
    //How often should enemy think
    public float thinkTime = .2f;
    private float nextThinkTime = 0f;

    //Food
    [Range(0,1)]
    public float foodWeight = .5f;
    [Range(0, 1)]
    public float foodXPWeight = .5f;
    [Range(0, 1)]
    public float foodDistWeight = .5f;

    //Enemy
    public bool attackWhileEating = false;
    [Range(0, 1)]
    public float enemyWeight = .5f;
    [Range(0, 1)]
    public float enemyXPWeight = .5f;
    [Range(0, 1)]
    public float enemyHPWeight = .5f;
    [Range(0, 1)]
    public float enemyDistWeight = .5f;

    //Reproduce
    [Range(0, 1)]
    public float reproduceWeight = .5f;
    [Range(0, 1)]
    public float reproduceHPWeight = .5f;
    [Range(0, 1)]
    public float reproduceXPWeight = .5f;

    //Upgrades
    [Range(0, 1)]
    public float offenseUpgradeWeight = .5f;
    [Range(0, 1)]
    public float defenseUpgradeWeight = .5f;
    [Range(0, 1)]
    public float mobilityUpgradeWeight = .5f;
    [Range(0, 1)]
    public float upgradePriceWeight = .5f;

    //Mutation
    [Range(0, 1)]
    public float mutationRate = .5f;

    private EntityUpgrades upgrades;

    private Food bestFood;
    private float bestFoodScore = float.MinValue;
    private Entity bestEnemy;
    private float bestEnemyScore = float.MinValue;
    private float reproduceScore;
    private EntityUpgrades.UpgradeType bestUpgrade;
    private float bestUpgradeScore;

    internal override void Awake()
    {
        base.Awake();

        upgrades = GetComponent<EntityUpgrades>();
    }

    internal override void TakeInput()
    {
        if (nextThinkTime > Time.time)
            return;

        nextThinkTime += thinkTime;

        CalculateFoodScore();
        CalculateEnemyScore();
        CalculateReproduceScore();
        CalculateUpgradeScore();

        if (bestEnemy == null && bestFood == null)
        {
            Move(Vector2.zero);
            return;
        }

        Vector3 target;

        if (bestUpgradeScore > reproduceScore)
        {
            upgrades.Upgrade((int)bestUpgrade);
        }

        if (reproduceScore > bestUpgradeScore && reproduceScore > bestEnemyScore)
        {
            Reproduce();
        }

        if ((bestFoodScore > bestEnemyScore || bestEnemy == null) && bestFood != null) //Go for food
        {
            target = bestFood.transform.position;
            if (attackWhileEating && bestEnemy != null)
            {
                Shoot(bestEnemy.transform.position - transform.position);
            }
        }
        else //Go for enemy
        {
            target = bestEnemy.transform.position;
            Shoot(target - transform.position);
        }

        Move(target - transform.position);
    }

    private void CalculateFoodScore()
    {
        bestFoodScore = 0f;
        Food[] allFood = FindObjectsOfType<Food>();
        foreach (Food food in allFood)
        {
            float sqrDist = (food.transform.position - transform.position).sqrMagnitude;
            float score =
                (1f / sqrDist * foodDistWeight
                + food.xp / 5f * foodXPWeight) / 2f;

            if (score > bestFoodScore)
            {
                bestFoodScore = score;
                bestFood = food;
            }
        }
        bestFoodScore *= foodWeight;
    }

    private void CalculateReproduceScore()
    {
        reproduceScore =
            (entity.currentHP / 10f * reproduceHPWeight
            + entity.currentXP / 100f * reproduceXPWeight) / 2f * reproduceWeight;
    }

    private void CalculateEnemyScore()
    {
        if (!entity.canShoot)
        {
            bestEnemyScore = 0;
            return;
        }

        bestEnemyScore = 0f;
        Entity[] allEnemies = 
            (from e in FindObjectsOfType<Entity>()
             where e.entityType != entity.entityType select e).ToArray();

        foreach (Entity enemy in allEnemies)
        {
            float sqrDist = (enemy.transform.position - transform.position).sqrMagnitude;
            float score =
                (1f / sqrDist * enemyDistWeight
                + Mathf.Clamp01(enemy.currentXP / 50f) * enemyXPWeight
                + 1f / enemy.currentHP * enemyHPWeight) / 3f;

            if (score > bestEnemyScore)
            {
                bestEnemyScore = score;
                bestEnemy = enemy;
            }
        }
        bestEnemyScore *= enemyWeight;
    }

    private void CalculateUpgradeScore()
    {
        float offenseScore = 0f, defenseScore = 0f, mobilityScore = 0f;

        if (entity.currentXP >= upgrades.offPrice)
            offenseScore = 1f / upgrades.offPrice * upgradePriceWeight * offenseUpgradeWeight;
        if (entity.currentXP >= upgrades.defPrice)
            defenseScore = 1f / upgrades.defPrice * upgradePriceWeight * defenseUpgradeWeight;
        if (entity.currentXP >= upgrades.mobPrice)
            mobilityScore = 1f / upgrades.mobPrice * upgradePriceWeight * mobilityUpgradeWeight;

        bestUpgradeScore = offenseScore;
        bestUpgrade = EntityUpgrades.UpgradeType.Offense;
        if (defenseScore > bestUpgradeScore)
        {
            bestUpgradeScore = defenseScore;
            bestUpgrade = EntityUpgrades.UpgradeType.Defense;
        }
        if (mobilityScore > bestUpgradeScore)
        {
            bestUpgradeScore = mobilityScore;
            bestUpgrade = EntityUpgrades.UpgradeType.Mobility;
        }
    }

    public void RandomizeWeights()
    {
        //Food
        foodWeight = UnityEngine.Random.value;
        foodDistWeight = UnityEngine.Random.value;
        foodXPWeight = UnityEngine.Random.value;
        //Enemy
        enemyWeight = UnityEngine.Random.value;
        enemyDistWeight = UnityEngine.Random.value;
        enemyXPWeight = UnityEngine.Random.value;
        enemyHPWeight = UnityEngine.Random.value;
        //Reproduce
        reproduceWeight = UnityEngine.Random.value;
        reproduceXPWeight = UnityEngine.Random.value;
        reproduceHPWeight = UnityEngine.Random.value;
        //Upgrades
        offenseUpgradeWeight = UnityEngine.Random.value;
        defenseUpgradeWeight = UnityEngine.Random.value;
        mobilityUpgradeWeight = UnityEngine.Random.value;
        upgradePriceWeight = UnityEngine.Random.value;
    }

    public void MutateWeights(EnemyController parent)
    {
        mutationRate = parent.mutationRate;

        //Food
        foodWeight = Mathf.Clamp01(parent.foodWeight + (UnityEngine.Random.value - .5f) * mutationRate);
        foodDistWeight = Mathf.Clamp01(parent.foodDistWeight + (UnityEngine.Random.value - .5f) * mutationRate);
        foodXPWeight = Mathf.Clamp01(parent.foodXPWeight + (UnityEngine.Random.value - .5f) * mutationRate);
        //Enemy
        enemyWeight = Mathf.Clamp01(parent.enemyWeight + (UnityEngine.Random.value - .5f) * mutationRate);
        enemyDistWeight = Mathf.Clamp01(parent.enemyDistWeight + (UnityEngine.Random.value - .5f) * mutationRate);
        enemyXPWeight = Mathf.Clamp01(parent.enemyXPWeight + (UnityEngine.Random.value - .5f) * mutationRate);
        enemyHPWeight = Mathf.Clamp01(parent.enemyHPWeight + (UnityEngine.Random.value - .5f) * mutationRate);
        //Reproduce
        reproduceWeight = Mathf.Clamp01(parent.reproduceWeight + (UnityEngine.Random.value - .5f) * mutationRate);
        reproduceXPWeight = Mathf.Clamp01(parent.reproduceXPWeight + (UnityEngine.Random.value - .5f) * mutationRate);
        reproduceHPWeight = Mathf.Clamp01(parent.reproduceHPWeight + (UnityEngine.Random.value - .5f) * mutationRate);
        //Upgrades
        offenseUpgradeWeight = Mathf.Clamp01(parent.offenseUpgradeWeight + (UnityEngine.Random.value - .5f) * mutationRate);
        defenseUpgradeWeight = Mathf.Clamp01(parent.defenseUpgradeWeight + (UnityEngine.Random.value - .5f) * mutationRate);
        mobilityUpgradeWeight = Mathf.Clamp01(parent.mobilityUpgradeWeight + (UnityEngine.Random.value - .5f) * mutationRate);
        upgradePriceWeight = Mathf.Clamp01(parent.upgradePriceWeight + (UnityEngine.Random.value - .5f) * mutationRate);
    }
}
