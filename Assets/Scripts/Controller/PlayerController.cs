﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Controller
{

    internal override void TakeInput()
    {
        //Movement
        float hor = Input.GetAxisRaw("Horizontal");
        float ver = Input.GetAxisRaw("Vertical");
        Vector2 moveDir = new Vector2(hor, ver);

        Move(moveDir);

        //Aim
        Vector2 aimDir = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;

        //Skills
        if (Input.GetButton("Dash"))
        {
            Dash();
        }
        if (Input.GetButton("Shoot"))
        {
            Shoot(aimDir);
        }
        if (Input.GetButton("Reproduce"))
        {
            Reproduce();
        }
    }
}
