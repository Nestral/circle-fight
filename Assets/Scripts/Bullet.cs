﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Bullet : Hitable
{
    [HideInInspector]
    public Entity.EntityType type;

    public float lifeSizeDec = .5f;
    public float damage = 1;

    internal override void SetSize()
    {
        if (damage <= minSize)
        {
            Destroy(gameObject);
            return;
        }

        transform.localScale = defaultScale + scaleInc * (damage - 1f);
    }

    private void Update()
    {
        damage -= lifeSizeDec * Time.deltaTime;
        SetSize();
    }

    internal override void StayInEntity(Collider2D other)
    {
        Entity entity = other.GetComponent<Entity>();
        if (entity.entityType == type)
            return;

        float dec = sizeDecSpeed * 20f * Time.deltaTime;
        if (dec > damage + minSize)
            dec = damage;

        damage -= dec;
        entity.DealDamage(dec);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Shield"))
        {
            Vector2 inNormal = (transform.position - other.transform.position).normalized;
            float angle = Vector2.Angle(inNormal, other.transform.up);

            if (angle < 88)
            {
                Rigidbody2D rb = GetComponent<Rigidbody2D>();
                rb.velocity = Vector2.Reflect(rb.velocity, inNormal);
            }
        }

    }
}
