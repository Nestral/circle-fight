﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityUpgrades : MonoBehaviour
{
    public enum UpgradeType { Offense, Defense, Mobility };

    [HideInInspector]
    public Entity entity;
    private PlayerController playerController;

    public int offenseLevel = 0;
    public int maxOffLevel = 5;
    public int offPrice = 10;
    public float offPriceMult = 1.5f;
    public float shootDamageInc = 1f;

    public int defenseLevel = 0;
    public int maxDefLevel = 3;
    public int defPrice = 10;
    public float defPriceMult = 1.5f;

    public int mobilityLevel = 0;
    public int maxMobLevel = 5;
    public int mobPrice = 10;
    public float mobPriceMult = 1.5f;
    public float dashSpeedInc = .5f;

    private void Awake()
    {
        entity = GetComponent<Entity>();

        Controller controller = GetComponent<Controller>();
        if (controller.GetType() == typeof(PlayerController))
            playerController = GetComponent<PlayerController>();
    }

    private void Start()
    {
        if (playerController != null)
            GameMenu.instance.UpdateUpgradesInfo(this);
    }

    public void Upgrade(int upgradeTypeIndex)
    {
        UpgradeType type = (UpgradeType)upgradeTypeIndex;

        if (type == UpgradeType.Defense && defenseLevel < maxDefLevel && entity.currentXP >= defPrice)
        {
            entity.SpendXP(defPrice);
            defenseLevel++;
            WorldGenerator.instance.SpawnShield(entity);

            defPrice = (int)(defPrice * defPriceMult);
        }
        else if (type == UpgradeType.Offense && offenseLevel < maxOffLevel && entity.currentXP >= offPrice)
        {
            entity.SpendXP(offPrice);
            entity.canShoot = true;
            offenseLevel++;
            entity.shootDamage += shootDamageInc;

            offPrice = (int)(offPrice * offPriceMult);
        }
        else if (type == UpgradeType.Mobility && mobilityLevel < maxMobLevel && entity.currentXP >= mobPrice)
        {
            entity.SpendXP(mobPrice);
            entity.canDash = true;
            mobilityLevel++;
            entity.dashSpeed += dashSpeedInc;

            mobPrice = (int)(mobPrice * mobPriceMult);
        }

        if (playerController != null)
            GameMenu.instance.UpdateUpgradesInfo(this);
    }
}
