﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldsHolder : MonoBehaviour
{
    [HideInInspector]
    public Entity owner;
    public float speed;
    public float distance;

    private float angle;

    public void Setup(Entity _owner, float _rotationSpeed, float _distance)
    {
        owner = _owner;
        speed = _rotationSpeed;
        distance = _distance;
    }

    void Update()
    {
        Rotate();
    }

    private void Rotate()
    {
        angle += speed * Time.deltaTime;

        Vector2 pos = owner.transform.position;
        Quaternion rot = Quaternion.Euler(0, 0, angle);
        transform.SetPositionAndRotation(pos, rot);
    }

    public void UpdateChildren()
    {
        float angleDif = 360 / transform.childCount;

        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            float x, y, childAngle = angleDif * i + angle;
            x = -Mathf.Sin(Mathf.Deg2Rad * childAngle);
            y = Mathf.Cos(Mathf.Deg2Rad * childAngle);
            Vector2 pos = new Vector2(x, y) * distance + (Vector2)transform.position;
            Quaternion rot = Quaternion.Euler(0, 0, childAngle);
            child.SetPositionAndRotation(pos, rot);
        }
    }
}
