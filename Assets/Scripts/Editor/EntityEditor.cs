﻿using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using System;

[CustomEditor(typeof(Entity)), CanEditMultipleObjects]
public class EntityEditor : Editor
{
    protected static bool showExtra = false;
    protected static bool showMove = false;
    protected static bool showShoot = false;
    protected static bool showDef = false;

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        GUILayout.Space(5);

        AlwaysGUI();

        HPXPGUI();

        MoveGUI();

        ShootGUI();

        DefenseGUI();

        serializedObject.ApplyModifiedProperties();
    }

    private void DefenseGUI()
    {
        showDef = EditorGUILayout.Foldout(showDef, "Defense");
        if (showDef)
        {
            EditorGUI.indentLevel++;

            EditorGUILayout.PropertyField(serializedObject.FindProperty("shieldsHolder"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("shieldSpeed"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("shieldDistance"));

            EditorGUI.indentLevel--;
        }
    }

    private void AlwaysGUI()
    {
        EditorGUILayout.PropertyField(serializedObject.FindProperty("entityType"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("currentHP"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("currentXP"));
    }

    private void HPXPGUI()
    {
        showExtra = EditorGUILayout.Foldout(showExtra, "HP & XP");
        if (showExtra)
        {
            EditorGUI.indentLevel++;

            EditorGUILayout.PropertyField(serializedObject.FindProperty("maxHP"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("startHP"));

            EditorGUI.indentLevel--;
        }
    }

    private void MoveGUI()
    {
        showMove = EditorGUILayout.Foldout(showMove, "Movement");
        if (showMove)
        {
            EditorGUI.indentLevel++;

            EditorGUILayout.PropertyField(serializedObject.FindProperty("moveSpeed"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("dashSpeed"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("dashTime"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("dashCooldown"));

            EditorGUI.indentLevel--;
        }
    }

    private void ShootGUI()
    {
        showShoot = EditorGUILayout.Foldout(showShoot, "Shooting");
        if (showShoot)
        {
            EditorGUI.indentLevel++;

            EditorGUILayout.PropertyField(serializedObject.FindProperty("bulletPrefab"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("shootDamage"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("shootSpeed"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("shootCooldown"));

            EditorGUI.indentLevel--;
        }
    }
}
