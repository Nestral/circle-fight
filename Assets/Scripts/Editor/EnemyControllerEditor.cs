﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(EnemyController))]
public class EnemyControllerEditor : Editor
{
    protected static bool showFood = false;
    protected static bool showEnemy = false;
    protected static bool showReproduce = false;
    protected static bool showUpgrade = false;

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        GUILayout.Space(5);

        AlwaysGUI();

        FoodGUI();

        EnemyGUI();

        ReproduceGUI();

        UpgradeGUI();

        serializedObject.ApplyModifiedProperties();
    }

    private void UpgradeGUI()
    {
        showUpgrade = EditorGUILayout.Foldout(showUpgrade, "Upgrade");

        if (showUpgrade)
        {
            EditorGUI.indentLevel++;

            EditorGUILayout.PropertyField(serializedObject.FindProperty("offenseUpgradeWeight"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("defenseUpgradeWeight"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("mobilityUpgradeWeight"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("upgradePriceWeight"));

            EditorGUI.indentLevel--;
        }
    }

    private void ReproduceGUI()
    {
        showReproduce = EditorGUILayout.Foldout(showReproduce, "Reproduce");

        if (showReproduce)
        {
            EditorGUI.indentLevel++;

            EditorGUILayout.PropertyField(serializedObject.FindProperty("reproduceWeight"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("reproduceHPWeight"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("reproduceXPWeight"));

            EditorGUI.indentLevel--;
        }
    }

    private void AlwaysGUI()
    {
        EditorGUILayout.PropertyField(serializedObject.FindProperty("thinkTime"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("mutationRate"));
    }

    private void FoodGUI()
    {
        showFood = EditorGUILayout.Foldout(showFood, "Food");

        if (showFood)
        {
            EditorGUI.indentLevel++;

            EditorGUILayout.PropertyField(serializedObject.FindProperty("foodWeight"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("foodXPWeight"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("foodDistWeight"));

            EditorGUI.indentLevel--;
        }
    }

    private void EnemyGUI()
    {
        showEnemy = EditorGUILayout.Foldout(showEnemy, "Enemy");

        if (showEnemy)
        {
            EditorGUI.indentLevel++;

            EditorGUILayout.PropertyField(serializedObject.FindProperty("attackWhileEating"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("enemyWeight"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("enemyXPWeight"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("enemyHPWeight"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("enemyDistWeight"));

            EditorGUI.indentLevel--;
        }
    }
}
