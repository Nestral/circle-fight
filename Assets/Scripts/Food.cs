﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : Hitable
{
    public float xp = 1;

    internal override void SetSize()
    {
        if (xp <= minSize)
        {
            Destroy(gameObject);
            return;
        }

        transform.localScale = defaultScale + scaleInc * (xp - 1);
    }

    internal override void StayInEntity(Collider2D other)
    {
        Entity entity = other.GetComponent<Entity>();

        float dec = sizeDecSpeed * 20f * Time.deltaTime;
        if (dec > xp + minSize)
            dec = xp;

        xp -= dec;
        entity.AddXP(dec);
    }
}
