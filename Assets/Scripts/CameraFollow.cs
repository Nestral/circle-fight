﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;
    public Vector3 offset;
    [Range(0,1)]
    public float smoothness;

    public float scrollSpeed = 500f;
    public float minSize = 2f;
    public float maxSize = 20f;

    private void Awake()
    {
        if (offset == Vector3.zero)
            offset = transform.position - target.position;
    }

    void FixedUpdate()
    {
        FollowTarget();

        TakeInput();
    }

    private void TakeInput()
    {
        float scroll = -Input.GetAxis("Mouse ScrollWheel");
        if (scroll != 0)
        {
            Camera.main.orthographicSize = Mathf.Clamp
                (Camera.main.orthographicSize + scroll * scrollSpeed * Time.deltaTime, minSize, maxSize);
        }
    }

    private void FollowTarget()
    {
        if (target == null)
            return;

        Vector3 targetPos = target.position + offset;
        Vector3 smoothPos = Vector3.Lerp(transform.position, targetPos, smoothness);

        transform.position = smoothPos;
    }
}
