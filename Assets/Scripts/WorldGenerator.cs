﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldGenerator : MonoBehaviour
{
    public static WorldGenerator instance;

    //Global
    public Transform spawnsHolder;
    public float difficulty = 1;
    public float difficultyInc = .01f;
    public float maxDifficulty = 10f;
    public Transform worldBorder;
    public float borderWidth = 3f;
    public Vector2 worldSize = new Vector2(10, 10);

    //Food spawning
    public Transform foodPrefab;
    public bool spawnFood = true;
    public float foodSpawnDelay = 1f;
    //Average amount of food spawned on a 50 by 50 square after foodSpawnDelay
    public float foodFrequency = 1f;
    private float nextFoodSpawnTime = 0f;
    private float foodSpawnFreq;


    //Enemies spawning
    public Transform enemyPrefab;
    public Transform friendPrefab;
    public bool spawnEnemies = false;
    public float enemySpawnDelay = 5f;
    private float nextEnemySpawnTime = 0f;

    //Shield
    public Transform shieldPrefab;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;

        foodSpawnFreq = (int)((worldSize.x / 50) * (worldSize.y / 50) * foodFrequency);
        if (foodSpawnFreq <= 0)
            foodSpawnFreq = 1;

        nextFoodSpawnTime = Time.time;
        nextEnemySpawnTime = Time.time + enemySpawnDelay;
    }

    private void Start()
    {
        //World border
        Vector2 modWorldSize = (worldSize + Vector2.one * borderWidth) / 2f;

        //Vertical
        worldBorder.localScale = new Vector3(borderWidth, worldSize.y + 2 * borderWidth);
        Instantiate(worldBorder, new Vector2(modWorldSize.x, 0), Quaternion.identity, spawnsHolder);
        Instantiate(worldBorder, new Vector2(-modWorldSize.x, 0), Quaternion.identity, spawnsHolder);

        //Horizontal
        worldBorder.localScale = new Vector3(worldSize.x, borderWidth);
        Instantiate(worldBorder, new Vector2(0, modWorldSize.y), Quaternion.identity, spawnsHolder);
        Instantiate(worldBorder, new Vector2(0, -modWorldSize.y), Quaternion.identity, spawnsHolder);
    }

    private void Update()
    {
        difficulty += difficultyInc * Time.deltaTime;

        if (spawnFood && nextFoodSpawnTime <= Time.time)
        {
            SpawnRandomFood();
            nextFoodSpawnTime = Time.time + foodSpawnDelay / foodSpawnFreq;
        }
        if (spawnEnemies && nextEnemySpawnTime <= Time.time)
        {
            SpawnRandomEnemy();
            nextEnemySpawnTime = Time.time + enemySpawnDelay;
        }
    }

    private void SpawnRandomFood()
    {
        SpawnFood(GetRandomPos(), Mathf.Clamp(difficulty, 1f, 5f));
    }

    public void SpawnFood(Vector2 pos, float xpValue)
    {
        Food newFood = Instantiate(foodPrefab, pos, Quaternion.identity, spawnsHolder).GetComponent<Food>();
        newFood.xp = xpValue;
    }

    private void SpawnRandomEnemy()
    {
        float hp = 9 + (int)difficulty;
        float xp = (int)difficulty * 2;
        SpawnEntity(null, Entity.EntityType.Enemy, GetRandomPos(), hp, xp);
    }

    public void SpawnEntity(EnemyController parentController, Entity.EntityType entityType, Vector2 pos, float hp, float xp)
    {
        Transform prefab = null;
        if (entityType == Entity.EntityType.Enemy)
            prefab = enemyPrefab;
        else if (entityType == Entity.EntityType.Friend)
            prefab = friendPrefab;

        if (prefab == null)
            return;

        Entity newEntity = Instantiate(prefab, pos, Quaternion.identity, spawnsHolder).GetComponent<Entity>();
        newEntity.entityType = entityType;
        newEntity.AddXP(xp);
        newEntity.startHP = hp;

        if (newEntity.maxHP < hp)
            newEntity.maxHP = hp;

        if (parentController == null)
            newEntity.GetComponent<EnemyController>().RandomizeWeights();
        else newEntity.GetComponent<EnemyController>().MutateWeights(parentController);
    }

    private Vector2 GetRandomPos()
    {
        float randX = (UnityEngine.Random.value - .5f) * worldSize.x;
        float randY = (UnityEngine.Random.value - .5f) * worldSize.y;
        Vector2 randomPos = new Vector2(randX, randY);
        return randomPos;
    }

    public void SpawnShield(Entity owner)
    {
        Transform newShield = Instantiate(shieldPrefab, owner.shieldsHolder);
        owner.shieldsHolder.GetComponent<ShieldsHolder>().UpdateChildren();
    }
}
